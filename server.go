package main

import (
	"fmt"
	goap "gitlab.com/activitygolang/go-activitystreams"
	"gitlab.com/activitygolang/go-activitypub/apnet"
)

// What is needed here:
//  * Interface with the AP core
//  * Dethreading layer over the interface
//  * Code to connect to other servers
//  * Network listener

// AP core interface requirements:
//  * conform to UpperLevel interface (and have some way of sending that data)

// Dethreading layer requirements:
//  * make sure the thing can't stall out

// Network sender requirements:

// Network listener requirements:
//  * Encapsulate network centric layers (TLS, auth)
//  * Do network level sanity checks on incoming objects

func main() {
	// get arguments and config
	// set up core
	_ = goap.NewCore() // apcore
	// set up network listener
	inter := NewCoreInterface()
	listener := apnet.NewListener(inter)
	// http.ListenAndServe()
}

type CallReturn struct {
	Error  error
	Object map[string]interface{} // Returned by Get
}

type CoreCall struct {
	Type       int // What call is being made?
	ObjectID   string
	ActorID    string
	Object     map[string]interface{}
	ReturnChan chan CallReturn
}

type CoreInterface struct {
	calls    chan CoreCall // internal dethreader channel
	Running bool
}

func NewCoreInterface() (c *CoreInterface) {
	c = &CoreInterface{}
	c.calls = make(chan CoreCall, 128) // hat generated number
	return c
}

func (c *CoreInterface) DethreaderLoop() { // stub
	for c.Running {
		select {
		case call := <-c.calls:
			fmt.Println(call)// compiler bait
			// call one of:
			//  * Get
			//  * PostInboxRemote
			// return result in provided channel
		}
	}
}

func (c *CoreInterface) Get(objectID string, authActorID string) (object map[string]interface{}, err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: 0, // TODO
		ObjectID: objectID,
		ActorID: authActorID,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Object, result.Error
}

func (c *CoreInterface) PostInbox(targetID string, object map[string]interface{}) (err error) {
	retChan := make(chan CallReturn, 1)
	call := CoreCall{
		Type: 0, // TODO
		ActorID: targetID,
		Object: object,
		ReturnChan: retChan,
	}
	c.calls <- call
	result := <-retChan
	return result.Error
}
