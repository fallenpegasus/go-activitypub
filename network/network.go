package apnet

import (
	"net/http"
)

type APInterface interface {
	// Get
	// Post
}

// Listener must do the upper level checks and conversions for GoAP
type Listener struct {
	core APInterface
}

func NewListener(inter APInterface) (l *Listener) {
	l = &Listener{}
	l.core = inter
	return l
}

func (l *Listener) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	// read all of the request first
	// if a get, send to coreinterface.Get
	// if a post, send to coreinterface.PostInbox
}

func (l *Listener) handleGet(resp http.ResponseWriter, req *http.Request) {
	// do top level goap sanity checks
	// get object
	// serialize object into body and return
}

func (l *Listener) handlePost(resp http.ResponseWriter, req *http.Request) {
	// do top level goap sanity checks
}
